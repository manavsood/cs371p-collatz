// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i;
    int j;
    tie(i, j) = p;
    // <your code>
    int max = 0;
    int ret = 0;
    int cache[3000001];//max val is 3 * 1000000 + 1

    if (i > 1000000 || j > 1000000) {//check boundaries
        return make_tuple(-1, -1, -1);
    }
    if (i > j) {
        int temp = j;
        j = i;
        i = temp;
    }
    for (int q = i; q <= j; q++) {
        long t = q;
        int counter = 1;//start at 1 because 1 number will be printed no matter what
        while (t > 1) {
            if (t % 2 == 1)
                t = 3 * t + 1;
            else
                t = t / 2;
            if (t > 0 && t < 3000001) {//check val before actually seeing its there in cache
                if (cache[t] > 0) {//if val is cached add its cycles and break
                    counter += cache[t];
                    break;
                }
            }
            counter++;
        }
        cache[q] = counter;
        if (counter > max) {//update max
            max = counter;
        }
    }
    ret = max;
    return make_tuple(i, j, ret);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
         collatz_print(sout, collatz_eval(collatz_read(s)));}
